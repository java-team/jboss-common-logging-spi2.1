#!/bin/sh

set -e

TAR=../jboss-common-logging-spi2.1_$2.orig.tar.gz
DIR=jboss-common-logging-spi-$2
TAG=$2

svn export http://anonsvn.jboss.org/repos/common/common-logging-spi/tags/$TAG $DIR
GZIP=--best tar -c -z -f $TAR --exclude '*.jar' --exclude '*.class' $DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir
  echo "moved $TAR to $origDir"
fi
